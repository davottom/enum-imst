#include <climits>
#include <iomanip>
#include <iostream>
#include <unistd.h>
#include <getopt.h>



struct  Options {

  void display_help() const {
    std::cout << "Available options:\n";
    std::cout << "--in       input file in dot format\n";
    std::cout << "--out      output file in dot format\n";
    std::cout << "--svg      generate a svg file from the output file (required graphviz)\n";
    std::cout << "--mode     enumerating algorithm, among the following\n";
    std::cout << "  *poly    polynomial algorithm in the size of the output (default)\n";
    std::cout << "  *approx  outer approximation, enumerate every spanning trees of the subgraph of possible edges \n";
    std::cout << "  *reduce  exact approach, first call the outer approximation then check for each spanning tree if it is non-dominated\n";
    std::cout << "--rand     generate random graphs (prompt for other inputs)\n";
    std::cout << "--bound    bound the number of enumerated trees\n";
    std::cout << "--color    color dominated trees when using outer approximation\n";
    std::cout << std::endl;
    exit(EXIT_SUCCESS);
  }

  enum Mode {
    enumerate, approx, approx_and_check, generate_random
  } ;
  
  std::string input, output, comp_file;
  bool generate_svg, color;
  int opt;
  Mode mode;
  unsigned bound;


  Options(int argc, char * argv[]) : input("-1"), output("-1"), mode(enumerate), generate_svg(false), bound(UINT_MAX), comp_file("-1")
  {
    option long_arg[] = {
      {"in", required_argument, nullptr, 'i'},
      {"out", required_argument,nullptr, 'o'},
      {"mode", required_argument, nullptr, 'm'},
      {"bound", required_argument, nullptr, 'b'},
      {"svg", no_argument, nullptr, 's'},
      {"help", no_argument, nullptr, 'h'},
      {"rand", no_argument,nullptr,'r'},
      {"color", no_argument,nullptr,'c'},
      {"compare", required_argument,nullptr, 't'}
	
    };

    std::string tmp;
    if(argc==1){
      std::cout << "No argument provided\n";
      display_help();
    }
    while((opt = getopt_long(argc, argv, "i:o:m:sh",long_arg,nullptr)) !=-1) {
      switch (opt)
	{
	case 'i':
	  input=optarg;
	  break;
	case 'o':
	  output=optarg;
	  break;
	case 'm':
	  tmp=optarg;
	  if(tmp=="approx")
	    mode=approx;
	  else if(tmp=="reduceApprox")
	    mode=approx_and_check;
	  break;
	case 's': 
	  generate_svg=true;
	  break;
	case 'r':
	  mode=generate_random;
	break;
	case 'b':
	  tmp=optarg;
	  bound=std::atoi(tmp.c_str());
	  break;
	case 'c':
	  color=true;
	  break;
	case 't':
	  comp_file=optarg;
	  break;
	  
	case 'h':
	default:
	  display_help();
	  break;
	}
    }
    if((mode==generate_random) && output=="-1"){
      std::cout << "Error: no output file provided\n";
      exit(EXIT_SUCCESS);
    }
    if(mode!=generate_random && input=="-1"){
      std::cout << "Error: no input file provided\n";
      exit(EXIT_SUCCESS);
    }
      
    
  }
};
