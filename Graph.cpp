#include "Graph.hpp"

const std::string & Edge::set_label() 
  {
    label="[";
    label.append(std::to_string(lowerWeight));
    label.append(",");
    label.append(std::to_string(upperWeight));
    label.append("]");
    return label;
  }


  void Edge::put_in(int s_id)
  {
    assert(out==false);
    in=true;
    step_id=s_id;
  }

  void Edge::put_out(int s_id)
  {
    assert(in==false);
    out=true;
    step_id=s_id;
  }

bool Edge::reset(int step_id)
  {
    if(this->step_id>=step_id){
      in=false;
      out=false;
      step_id=-1;
      return true;
    }
    return false;
  }


void Graph::read_dot(std::string fileName)
{
    
  boost::dynamic_properties dp(boost::ignore_other_properties);
  dp.property("node_id", boost::get(&Vertex::id, *this));
  dp.property("low", boost::get(&Edge::lowerWeight,*this));
  dp.property("up", boost::get(&Edge::upperWeight,*this));

  std::ifstream dot(fileName);

  bool status = boost::read_graphviz(dot,*this,dp);

  for(t_edge &e : m_edges){
    sorted_edge_low.push_back(&e);
  }
    
  std::sort(sorted_edge_low.begin(),sorted_edge_low.end(),[](t_edge *e1,t_edge *e2){return e1->m_property.lowerWeight<e2->m_property.lowerWeight;});
}

void Graph::reset_connected_components()
{
  for(auto &v : this->m_vertices)
    v.m_property.connected_component=v.m_property.id;
}

bool Graph::is_bridge(t_edge *uv){
  reset_connected_components();
  for(t_edge *e : sorted_edge_low){
    if(e->m_property.out || e==uv)
      continue;
      
    size_t cc1=(*this)[e->m_source].connected_component,
      cc2=(*this)[e->m_target].connected_component;
      
    if(cc1==cc2) continue;
    if(cc1>cc2) std::swap(cc1,cc2);
      
    for(auto &v :(*this).m_vertices){
      if(v.m_property.connected_component==cc2)
	v.m_property.connected_component=cc1;
    }
  }

  return (*this)[uv->m_source].connected_component!=(*this)[uv->m_target].connected_component;
}

bool Graph::close_cycle(t_edge *uv){
  reset_connected_components();
  for(t_edge *e : sorted_edge_low){
    if(!e->m_property.in || e==uv)
      continue;
      
    size_t cc1=(*this)[e->m_source].connected_component,
      cc2=(*this)[e->m_target].connected_component;
      
    if(cc1==cc2) continue;
    if(cc1>cc2) std::swap(cc1,cc2);
      
    for(auto &v :(*this).m_vertices){
      if(v.m_property.connected_component==cc2)
	v.m_property.connected_component=cc1;
    }
  }
  return (*this)[uv->m_source].connected_component==(*this)[uv->m_target].connected_component;
}



bool Graph::is_possible(t_edge *uv)
{
  for(t_edge *e : sorted_edge_low){
    if(e->m_property.out ||
       (!e->m_property.in && uv->m_property.lowerWeight<=e->m_property.upperWeight) ||
       e==uv)
      continue;
      
    size_t cc1=(*this)[e->m_source].connected_component,
      cc2=(*this)[e->m_target].connected_component;
      
    if(cc1==cc2) continue;
    if(cc1>cc2) std::swap(cc1,cc2);
      
    for(auto &v :(*this).m_vertices){
      if(v.m_property.connected_component==cc2)
	v.m_property.connected_component=cc1;
    }
  }
  return (*this)[uv->m_source].connected_component!=(*this)[uv->m_target].connected_component;
}


bool Graph::is_necessary(t_edge *uv)
{
  // reset_connected_components();
  for(t_edge *xy : sorted_edge_low){
    if(uv!=xy &&
       (!xy->m_property.out || xy->m_property.upperWeight<uv->m_property.lowerWeight))
      continue;

    reset_connected_components();
    for(t_edge *e : sorted_edge_low){
      if(e->m_property.out || 
	 (!e->m_property.in && xy->m_property.upperWeight<e->m_property.lowerWeight) ||
	 e==uv || e==xy)
	continue;
      
      size_t cc1=(*this)[e->m_source].connected_component,
	cc2=(*this)[e->m_target].connected_component;
      
      if(cc1==cc2) continue;
      if(cc1>cc2) std::swap(cc1,cc2);
      
      for(auto &v :(*this).m_vertices){
	if(v.m_property.connected_component==cc2)
	  v.m_property.connected_component=cc1;
      }
    }
    size_t cc_u=(*this)[uv->m_source].connected_component,
      cc_v=(*this)[uv->m_target].connected_component,
      cc_x=(*this)[xy->m_source].connected_component,
      cc_y=(*this)[xy->m_target].connected_component;

    if(cc_u!=cc_v && ((cc_v==cc_x && cc_u==cc_y) || (cc_u==cc_x && cc_v==cc_y)))
      return true;
  }

  return false;
}

void Graph::print_tree(std::ofstream &dot, unsigned treeId, bool color) const
{
  dot << std::endl;
  dot << "subgraph cluster_T" << treeId  <<"{" <<std::endl;
  dot <<"label=\"T" << treeId << "\"" << std::endl;
  if(color){
    dot << "style=filled;\ncolor=lightgrey;\n";
  }
  for(auto &v : this->m_vertices)
    dot<< v.m_property.id+this->m_vertices.size()*(treeId-1)<<"[vertex_id=" << v.m_property.id
       << ",label=" << v.m_property.id
       << "];" << std::endl;

  for(auto e : this->sorted_edge_low)
    if(e->m_property.in)
      dot << (*this)[e->m_source].id+this->m_vertices.size()*(treeId-1) << "--"
	  << (*this)[e->m_target].id+this->m_vertices.size()*(treeId-1)
	  << "[label=\"" << e->m_property.set_label()
	  << "\",low="   << e->m_property.lowerWeight
	  << ",up="    << e->m_property.upperWeight
	  << "];"      <<std::endl;
	  
    
  dot << "}\n";

}

void Graph::compute_possible_edges(int step_id)
{

  for(auto e : sorted_edge_low){
    reset_connected_components();
    if(e->m_property.in ||e->m_property.out) continue;
    if(!is_possible(e)){
      e->m_property.put_out(step_id);
    }
  }

}

void Graph::compute_necessary_edges(int step_id)
{
  for(auto e : sorted_edge_low){
    if(e->m_property.in ||e->m_property.out) continue;
    if(is_necessary(e)){
      e->m_property.put_in(step_id);
    }
  }
}

void Graph::compute_bridges(int step_id)
{
  for(auto e : sorted_edge_low){
    if(e->m_property.in ||e->m_property.out) continue;
    if(is_bridge(e)){
      e->m_property.put_in(step_id);
    }
  }
}

void Graph::compute_closing_cycle_edges(int step_id)
{
  for(auto e : sorted_edge_low){
    if(e->m_property.in ||e->m_property.out) continue;
    if(close_cycle(e)){
      e->m_property.put_out(step_id);
    }
  }
}
  
bool Graph::check_tree()
{
  std::vector<t_edge*> order = sorted_edge_low;

  std::sort(order.begin(), order.end(),
	    [](const t_edge *e1, const t_edge *e2) {
	      unsigned w_e1 = e1->m_property.in ? e1->m_property.lowerWeight : e1->m_property.upperWeight;
	      unsigned w_e2 = e2->m_property.in ? e2->m_property.lowerWeight : e2->m_property.upperWeight;
	      if(w_e1<w_e2) return true;
	      return w_e1==w_e2 && e1->m_property.in && !e2->m_property.in;
	    });

  reset_connected_components();
  for(t_edge *e : order){
      
    if(e->m_property.out && (*this)[e->m_source].connected_component !=(*this)[e->m_target].connected_component) return false;

    if(e->m_property.out) continue;
      
    size_t cc1=(*this)[e->m_source].connected_component,
      cc2=(*this)[e->m_target].connected_component;
      
    if(cc1==cc2) return false;
    if(cc1>cc2) std::swap(cc1,cc2);
      
    for(auto &v :(*this).m_vertices){
      if(v.m_property.connected_component==cc2)
	v.m_property.connected_component=cc1;
    }
  }

  return true;
}

Graph::Graph(std::string fileName) : graph_p()
{
  read_dot(fileName);
}


				  
unsigned Graph::enumerate_outer_approximation(std::string file, unsigned bound, unsigned step_approx,bool exact, bool color)
{
  for(auto &e : sorted_edge_low){
    int old=e->m_property.step_id;
    bool tmp=e->m_property.reset(-2);
  }
  std::ofstream dot;
  if(file!="-1"){
    dot.open(file);
    dot << "graph G {" << std::endl;
  }
    
  size_t nb_tree=0;
  compute_necessary_edges();
  compute_possible_edges();

  std::stack<std::pair<bool,unsigned>> stack;

  unsigned step=0;
  //select the first possible edge that is not necessary
  for(step=0;step<sorted_edge_low.size() && (sorted_edge_low[step]->m_property.out || sorted_edge_low[step]->m_property.in);step++);

  stack.push(std::make_pair(false, step));
  stack.push(std::make_pair(true, step));
    
  while(!stack.empty()) {
    auto p= stack.top();
    stack.pop();

    t_edge *e= sorted_edge_low[p.second];
      
    if(!p.first)
      for(auto &e : sorted_edge_low){
	int old=e->m_property.step_id;
	bool tmp=e->m_property.reset(p.second);
	  
      }
      
    if(p.first)
      e->m_property.put_in(p.second);
    else e->m_property.put_out(p.second);

    if(step>step_approx){
      compute_bridges(p.second);
      compute_closing_cycle_edges(p.second);
    }
    else {
      compute_necessary_edges(p.second);
      compute_possible_edges(p.second);
    }

    // go to the next possible edge that is not necessary
    for(step=p.second+1;
	step<sorted_edge_low.size() && (sorted_edge_low[step]->m_property.out || sorted_edge_low[step]->m_property.in)
	  ;++step);
    if(step==sorted_edge_low.size()){
      bool check=false;
      if(step>step_approx && (exact || color))
	check=check_tree();
	
      if(!exact || check){
	nb_tree++;
	if(file!="-1" && nb_tree<=bound){
	  print_tree(dot,nb_tree,color && !check);
	}
      }
    }
    else {
      stack.push(std::make_pair(false, step));
      stack.push(std::make_pair(true, step));
    }
  }
  if(file!="-1"){
    dot << "\n}\n";
    dot.close();
  }
  return nb_tree;

}

  
unsigned Graph::enumerate_non_dominated_trees(std::string file, unsigned bound)
{
  for(auto &e : sorted_edge_low){
    int old=e->m_property.step_id;
    bool tmp=e->m_property.reset(-2);
  }

  std::ofstream dot;
  if(file!="-1"){
    dot.open(file);
    dot << "graph G {" << std::endl;
  }
    
  size_t nb_tree=0;

  compute_necessary_edges();
  compute_possible_edges();

  std::stack<std::pair<bool,unsigned>> stack;

  unsigned step=0;

  for(auto e : sorted_edge_low){
    reset_connected_components();
    if(e->m_property.in ||e->m_property.out) continue;
    if(!is_possible(e)){
      e->m_property.put_out(step);
      step++;
      continue;
    }
    if(is_necessary(e)){
      e->m_property.put_in(step);
      step++;
      continue;
    }
    break;
  }
  
  //select the first possible edge that is not necessary
  for(step=0;step<sorted_edge_low.size() && (sorted_edge_low[step]->m_property.out || sorted_edge_low[step]->m_property.in);step++);

  stack.push(std::make_pair(false, step));
  stack.push(std::make_pair(true, step));
    
  while(!stack.empty()) {
    auto p= stack.top();
    stack.pop();


    t_edge *e= sorted_edge_low[p.second];
      
    if(!p.first)
      for(auto &e : sorted_edge_low){
	int old=e->m_property.step_id;
	bool tmp=e->m_property.reset(p.second);
      }
      
    if(p.first){
      e->m_property.put_in(p.second);
    }
    else {
      e->m_property.put_out(p.second);
    }

    for(auto e : sorted_edge_low){
      reset_connected_components();
      if(e->m_property.in ||e->m_property.out) continue;
      if(!is_possible(e)){
	e->m_property.put_out(p.second);
	continue;
      }
      if(is_necessary(e)){
	e->m_property.put_in(p.second);
	continue;
      }
      break;
    }
    

    // go to the next possible edge that is not necessary
    for(step=p.second+1;
	step<sorted_edge_low.size() && (sorted_edge_low[step]->m_property.out || sorted_edge_low[step]->m_property.in)
	  ;++step);
      
    if(step==sorted_edge_low.size()){
      nb_tree++;
      if(file!="-1" && nb_tree<=bound)
	print_tree(dot,nb_tree);
	
      std::vector<bool> solution;
      for(auto e : sorted_edge_low){
	solution.push_back(e->m_property.in);
      }
      solutions.push_back(solution);
	  
	
    }
    else {
      stack.push(std::make_pair(false, step));
      stack.push(std::make_pair(true, step));
    }
  }
  if(file!="-1"){
    dot << "\n}\n";
    dot.close();
  }
  return nb_tree;
}

