#include <boost/graph/random.hpp>
#include <chrono>
#include <random>

#include "Generate.hpp"

std::pair<unsigned,unsigned> distribution::generate_interval() const
  {
    std::random_device rd;
    std::mt19937 gen(rd());

    std::uniform_int_distribution<> distrib_value(valueLowMin, valueLowMax);
    std::uniform_int_distribution<> distrib_size(sizeMin, sizeMax);
      
    size_t min=distrib_value(gen);
    size_t max=min+distrib_size(gen);
    return std::make_pair(min,max);
    
  }

void write_dot(std::string fileName,graph_p &g)
{
  for(auto &e : g.m_edges)
    e.m_property.set_label();

  boost::dynamic_properties dp(boost::ignore_other_properties);
  dp.property("node_id", boost::get(&Vertex::id, g));
  dp.property("low", boost::get(&Edge::lowerWeight,g));
  dp.property("up", boost::get(&Edge::upperWeight,g));
  dp.property("label", boost::get(&Edge::label,g));

  std::ofstream dot(fileName);
  boost::write_graphviz_dp(dot,g,dp);
}

graph_p generate_random(size_t n,size_t m, distribution d)
{
  typedef std::chrono::high_resolution_clock myclock;
  myclock::time_point beginning = myclock::now();
  
  myclock::duration duration = myclock::now() - beginning;
  unsigned seed2 = duration.count();
  std::mt19937 prng(seed2);
  graph_p g;
  generate_random_graph(g, n,m, prng,false);


  size_t order=1;
  for(auto &v : g.m_vertices){
    v.m_property.id=order++;
  }

  bool connected=false;
  while (!connected) {
    for(auto &v : g.m_vertices)
      v.m_property.connected_component=v.m_property.id;
    
  
    for(auto &e : g.m_edges){
      size_t cc1=g[e.m_source].connected_component,
	cc2=g[e.m_target].connected_component;
      
      if(cc1==cc2) continue;
      if(cc1>cc2) std::swap(cc1,cc2);
      
      for(auto &v :g.m_vertices){
	if(v.m_property.connected_component==cc2)
	  v.m_property.connected_component=cc1;
      }
    }
    bool tmp=false;
    int i=0;
    for(auto it=g.vertex_set().begin();it!=g.vertex_set().end()&& !tmp;++it){
      if(g.m_vertices.at(i).m_property.connected_component!=1){
	tmp=true;
	auto it2=g.vertex_set().begin();
	for(size_t j=0;j<i-1;++j)
	  ++it2;
	graph_p::vertex_descriptor v =*g.vertex_set().begin();
	boost::add_edge(*it,*it2,g);
      }
      i++;
    }
    if(tmp==false) connected=true;
  }
    
  for(auto &e : g.m_edges){
    auto p = d.generate_interval();
    e.m_property.lowerWeight=p.first;
    e.m_property.upperWeight=p.second;
  }
  
    
  return g;
}  

