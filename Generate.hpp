#include "Graph.hpp"

struct distribution {
  size_t valueLowMin,valueLowMax, sizeMin, sizeMax;

  std::pair<unsigned,unsigned> generate_interval() const;
};


void write_dot(std::string fileName,graph_p &g);

graph_p generate_random(size_t n,size_t m, distribution d);
                      
