#!/bin/bash
in_fold="data/order12/"
for fold in $in_fold/*; do
    out=$fold/stats.txt
    if [ -f "$out" ]; then
	    
	    rm $out
    fi
    echo $fold
    for file in $fold/*.dot
    do
	filename=$(basename -- "$file" .dot)
        echo $filename
#	echo "bin/enum-mst --in=\"$file\" --comp=$out"
        bin/enum-mst --in="$file" --comp=$out
    done
done
