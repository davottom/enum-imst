#include "Generate.hpp"
#include "options.hpp"
#include <chrono>
#include <climits>
#include <cstddef>
#include <cstdlib>
#include <string>


void avg_time(std::chrono::microseconds t, std::ofstream &ofs)
{
  int minutes=0, seconds=0, milliseconds=0, microseconds=0;

  minutes = std::chrono::duration_cast<std::chrono::minutes>(t).count();
  seconds = std::chrono::duration_cast<std::chrono::seconds>(t).count() % 60;
  std::cout << std::chrono::duration_cast<std::chrono::seconds>(t).count() << std::endl;
  milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(t).count() % 1000;
  microseconds = t.count() % 1000;
  if(minutes!=0)
    ofs<<minutes<<"m";
  if(seconds!=0 || minutes!=0)
    ofs<<seconds<<"s";
  if(milliseconds!=0 || minutes!=0 || seconds !=0)
    ofs<<milliseconds<<"ms";
  ofs<<microseconds<<std::endl;
}


void write_stats(std::string name_instance,std::string file, size_t exact, size_t approx, size_t reduce, std::chrono::microseconds total_time_exact, std::chrono::microseconds total_time_approx,std::chrono::microseconds total_time_reduce)
{
  std::cout << "Writing stats in " << file << std::endl;
  std::ofstream ofs;
  ofs.open(file, std::ofstream::out | std::ofstream::app);

  ofs << std::string(name_instance.length()+20,'*') << std::endl;
  ofs << "*" <<std::string(name_instance.length()+18,' ') << "*" << std::endl;
  ofs << "*         " << name_instance << "         *" << std::endl;
  ofs << "*" <<std::string(name_instance.length()+18,' ') << "*" << std::endl;
  ofs << std::string(name_instance.length()+20,'*') << std::endl;

  ofs<< "Number non-dominated trees:          " << exact <<std::endl;
  ofs<< "Number non-dominated trees (reduce): " << reduce <<std::endl;
  ofs<< "Size approx:                         " << approx << std::endl;
  ofs<< "Diff: " << approx-exact << std::endl;
  ofs<< std::endl;

  ofs<< "Average time exact:  ";
  avg_time(total_time_exact, ofs);
  ofs<< "Average time approx: ";
  avg_time(total_time_approx, ofs);
  ofs<< "Average time reduce: ";
  avg_time(total_time_reduce, ofs);
  std::cout << std::endl;

  
  
}


int main(int argc, char *argv[])
{
  Options options(argc,argv);
  if(options.mode==Options::generate_random){
    size_t nb_graphes;
    std::cout << "Number of generated graphes?" << std::endl;
    std::cin >> nb_graphes;
    size_t nb_vertices,nb_edges;
    std::cout << "Number of vertices?" << std::endl;
    std::cin >> nb_vertices;
    std::cout << "Number of edges?" << std::endl;
    std::cin >> nb_edges;

    distribution d;
    std::cout << "Min/Max value for centers of intervals" << std::endl;
    std::cin >> d.valueLowMin >> d.valueLowMax;
    std::cout << "Min/Max value for sizes of intervals" << std::endl;
    std::cin >> d.sizeMin >> d.sizeMax;

    std::string file = options.output;
    if(options.output.substr(options.output.size()-4,options.output.size()-1)==".dot")
      file = file.substr(0, file.find_last_of("."));

    for(unsigned i=1;i<=nb_graphes;++i){
      std::string file_tmp=file;
      file_tmp.append("_");
      file_tmp.append(std::to_string(i));
      std::string dot=file_tmp;
      dot.append(".dot");
      std::cout << dot << std::endl;
      graph_p g =generate_random(nb_vertices, nb_edges, d);
      write_dot(dot,g);
      if(options.generate_svg){
	std::string command = "dot -Tsvg ";
	file_tmp.append(".svg");
	command.append(dot);
	command.append(" > ");
	command.append(file_tmp);
	int r=system(command.c_str());
      }
    }
    
    exit(EXIT_SUCCESS);
  }

  if(options.comp_file!="-1"){
    Graph g(options.input);
    std::chrono::microseconds total_time_exact(0), total_time_approx(0), total_time_reduce(0);
    size_t exact, approx, reduce;
    size_t nb_runs=1;
    for(size_t i=0;i<nb_runs;++i){
      auto start = std::chrono::steady_clock::now();
      exact=g.enumerate_non_dominated_trees("-1",UINT_MAX);
      auto end = std::chrono::steady_clock::now();
      std::chrono::duration<double> diff = end - start;
      total_time_exact+=std::chrono::duration_cast<std::chrono::microseconds>(end - start);

      start = std::chrono::steady_clock::now();
      approx=g.enumerate_outer_approximation("-1",UINT_MAX,0);
      end = std::chrono::steady_clock::now();
      diff = end - start;
      total_time_approx+=std::chrono::duration_cast<std::chrono::microseconds>(end - start);

      start = std::chrono::steady_clock::now();
      reduce=g.enumerate_outer_approximation("-1",UINT_MAX,0,true);
      end = std::chrono::steady_clock::now();
      diff = end - start;
      total_time_reduce+=std::chrono::duration_cast<std::chrono::microseconds>(end - start);

    }
    total_time_exact/=nb_runs;
    total_time_approx/=nb_runs;
    total_time_reduce/=nb_runs;

    write_stats(options.input,options.comp_file, exact, approx, reduce, total_time_exact, total_time_approx, total_time_reduce);
    
    exit(EXIT_SUCCESS);
  }

  
  Graph g(options.input);
  std::string file = options.output;

  if(options.output!="-1" && options.output.substr(options.output.size()-4,options.output.size()-1)==".dot")
    file = file.substr(0, file.find_last_of("."));
  file.append(".svg");

  
  switch (options.mode) {
  case Options::enumerate:
    std::cout << "Number of enumerated trees: " 
	      << g.enumerate_non_dominated_trees(options.output, options.bound)
	      << std::endl;
    break;
  case Options::approx:
    std::cout << "Number of enumerated trees: " 
	      << g.enumerate_outer_approximation(options.output,
						 options.bound,0,false,options.color)
	      << std::endl;


    break;
  case Options::approx_and_check:
    std::cout << "Number of enumerated trees: " 
	      << g.enumerate_outer_approximation(options.output, options.bound,0,true)
	      << std::endl;


    break;
  default:
    break;
    
  }
 
  if(options.generate_svg){
    std::string command = "dot -Tsvg ";
    command.append(options.output);
    command.append(" > ");
    command.append(file);
    int r=system(command.c_str());
  }
  return 0;
}
