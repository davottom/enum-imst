#+TITLE: Enum-imst
#+DESCRIPTION: Enumerate minimum spanning trees under interval uncertainty
#+AUTHOR: Tom Davot
#+OPTIONS: num:t
#+STARTUP: overview

* Description
This project implements the exact enumerating algorithm described in our article /On the enumeration of non-dominated spanning trees of a graph with imprecise weights/.

* Pre-requisites
+ [[https://www.boost.org/doc/libs/1_71_0/more/getting_started/unix-variants.html][Boost Graph Library (BGL)]] 1.59 or above
+ [[https://graphviz.org/download/][graphviz]]

* Compilation
 Create a *makefile* using *cmake*

  #+BEGIN_SRC bash
     $ cmake .
   #+END_SRC

If everything went smoothly with *cmake*, compile the project using *make*

  #+BEGIN_SRC bash
     $ make
   #+END_SRC

* Usage
The executable should be produced in the *bin* directory.

  #+BEGIN_SRC bash
     $ bin/enum-imst [options]
   #+END_SRC

The accepted options are the following:
+ *--in=<arg>*             input file in dot format
+ *--out=<arg>*            output file in dot format
+ *--svg*            generate a svg file from the output file
+ *--mode=<arg>*     enumerating algorithm, arg can take the following values
  - /poly/           polynomial algorithm in the size of the output (default)
  - /approx/         outer approximation, enumerate every spanning trees of the subgraph of possible edges
  - /reduce/         exact approach, first call the outer approximation then check for each spanning tree if it is non-dominated;
+ *--rand*           generate random graphs (prompt for other inputs)
+ *--bound=<arg>*    bound the number of enumerated trees
+ *--color*          color dominated trees when using outer approximation

** Example
The following command enumerates every non-dominated trees of the graph /data/small_example/in/ex1.dot/
  #+BEGIN_SRC bash
     $ bin/enum-imst --in="data/small_example/in/ex1.dot" --out="data/small_example/out/ex1_result.dot" --svg
   #+END_SRC
** Script
+ To reproduce the tests of the conference article [[https://link.springer.com/chapter/10.1007/978-3-031-45608-4_26][On the Enumeration of Non-dominated Spanning Trees with Imprecise Weights, Davot et al., ECSQARU23]], you can use the script /scripts/comp_ECSQARU.sh/
+ To reproduce the tests of the journal article, you can use the script /scripts/comp_IJAR.sh/
