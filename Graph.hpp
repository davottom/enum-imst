#include <algorithm>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/detail/adjacency_list.hpp>
#include <boost/graph/detail/edge.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_selectors.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/graphviz.hpp>
#include <cassert>
#include <climits>
#include <cstddef>
#include <ios>
#include <string>
#include <utility>
#include <iostream>
#include <vector>



struct Vertex {
  size_t id, connected_component;
};

struct Edge {
  bool in, out;
  int step_id;

public:
  unsigned lowerWeight, upperWeight;
  std::string label;
  
  const std::string & set_label();

  void put_in(int s_id);

  void put_out(int s_id);

  bool reset(int step_id);

  Edge() : in(false), out(false), step_id(-1) {}
};




using graph_p = boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, Vertex, Edge>;


class Graph : public graph_p
{

public:
  typedef boost::list_edge<unsigned long, Edge> t_edge;
private:
  std::vector<t_edge*> sorted_edge_low; // todo create container inside the graph
  std::vector<std::vector<bool>> solutions;


  void read_dot(std::string fileName);
  
  void reset_connected_components();

  bool is_bridge(t_edge *uv);

  bool close_cycle(t_edge *uv);


  bool is_possible(t_edge *uv);


  bool is_necessary(t_edge *uv);

  void print_tree(std::ofstream &dot, unsigned treeId, bool color=false) const;

  void compute_possible_edges(int step_id=-1);

  void compute_necessary_edges(int step_id=-1);

  void compute_bridges(int step_id=-1);

  void compute_closing_cycle_edges(int step_id=-1);

  bool check_tree();
 
public:

  Graph(std::string fileName);
  
				  
  unsigned enumerate_outer_approximation(std::string file, unsigned bound, unsigned step_approx,bool exact=false, bool color=false);

  
  unsigned enumerate_non_dominated_trees(std::string file, unsigned bound);

  



};



